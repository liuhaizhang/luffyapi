#从 rest_framework 中找到'rest_framework.views.exception_handler', 重写该方法
from rest_framework.views import exception_handler
from .response import ApiResponse #导入重写的response
from .logger import log
def comd_exception_handler(exc, context):
    '''
    可以写得很复杂，具体到某个异常，我们在这里就不那么的仔细了。
    在这里主要是要加日志
    '''
    #记录日志
    log.error('出错view是:%s 错误是%s'%(str(context['view'].__class__.__name__),str(exc)))
    ret = exception_handler(exc,context)
    #ret 是一个Response对象，里面有一个data属性，
    #drf处理的，保存到ret
    if not ret:
        return ApiResponse(code=0,msg='errors',result=str(exc))
    else:
        return ApiResponse(code=1,result=ret.data,msg='errors')
