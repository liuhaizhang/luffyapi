from django.db import models

# Create your models here.
from django.contrib.auth.models import AbstractUser
class User(AbstractUser):
    #继承原来的auth表，拓展字段，不要与原来有的字段名重复
    telephone = models.CharField(max_length=11)
    icon = models.ImageField(upload_to='icon',default='/icon/default.jpg/')