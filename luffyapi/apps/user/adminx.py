import xadmin
from xadmin import views
'''
拓展auth的用户表不需要再注册了
'''
class GlobalSettings(object):
    site_title='xxx公司内部管理系统' #站点标题
    site_footer='xxx公司' #页脚
    menu_style='accordion'#设置菜单折叠
xadmin.site.register(views.CommAdminView,GlobalSettings)

