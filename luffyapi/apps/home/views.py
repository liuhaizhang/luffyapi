from django.shortcuts import render

# Create your views here.

from rest_framework.views import APIView
from luffyapi.utils.response import ApiResponse
from rest_framework.generics import GenericAPIView
from rest_framework.mixins import ListModelMixin,DestroyModelMixin,RetrieveModelMixin,UpdateModelMixin,CreateModelMixin
from rest_framework.viewsets import GenericViewSet

from rest_framework.generics import ListAPIView,ListCreateAPIView,DestroyAPIView,UpdateAPIView,RetrieveUpdateDestroyAPIView,RetrieveAPIView,RetrieveUpdateAPIView,RetrieveDestroyAPIView,CreateAPIView

from . import models
from . import serializer
from django.conf import settings
class Test(APIView):
    def get(self,request):
        age = request.GET.get('age')
        dci={'sge':11}
        print(dci,11,1,1,1,11)

        return ApiResponse(code=100,result=dci,msg='成功',headers={'Access-Control-Allow-Origin':'*'},age=age)

class Banner(GenericViewSet,ListModelMixin):
    queryset = models.Banner.objects.filter(is_delete=False,is_show=True).order_by('display_order')[:settings.BANNER_COUNT]
    #只展示两条数据
    serializer_class = serializer.BannerSerializer



