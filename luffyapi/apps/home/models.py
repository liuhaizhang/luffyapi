from django.db import models
from luffyapi.utils.models import BaseModel
#导入utils文件夹的基表


class Banner(BaseModel):
    img = models.ImageField(verbose_name='轮播图',help_text='图片尺寸3840x800',upload_to='banner',null=True)
    link = models.CharField(max_length=64,verbose_name='轮播图跳转的地址')
    info = models.TextField(verbose_name='简介',null=True)
    name = models.CharField(max_length=32,verbose_name='图片名字')
    def __str__(self):
        return self.name



